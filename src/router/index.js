import About from "../pages/About";
import Posts from "../pages/Posts";
import PostDetail from "../pages/PostDetail";
import Login from "../pages/Login";
import Color from "../pages/Color";
import Game from "../pages/Game";

export const privateRoutes = [
  {
    path: "/about",
    component: <About />,
    exact: true,
  },
  {
    path: "/posts",
    component: <Posts />,
    exact: true,
  },
  {
    path: "/posts/:id",
    component: <PostDetail />,
    exact: true,
  },

  {
    path: "/colors",
    component: <Color />,
    exact: true,
  },
  {
    path: "/game",
    component: <Game />,
    exact: true,
  },
];

export const publicRoutes = [
  {
    path: "/login",
    component: <Login />,
    exact: true,
  },
];
