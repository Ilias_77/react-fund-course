import React, { useEffect, useState } from "react";
import Board from "../components/TicTacToe/Board";
import MyButton from "../components/UI/button/MyButton";

function Game() {
  const [history, setHistory] = useState([
    {
      squares: Array(9).fill(null),
      squareIndex: null,
    },
  ]);
  const [xIsNext, setXIsNext] = useState(true);
  const [stepNumber, setStepNumber] = useState(0);
  const [selectedStepSquareIndex, setSelectedStepSquareIndex] = useState();

  const [movesOrderDirection, setMovesOrderDirection] = useState("asc");
  const [orderedMoves, setOrderedMoves] = useState();

  function handleClick(i) {
    const tmpHistory = history.slice(0, stepNumber + 1);
    const current = tmpHistory[tmpHistory.length - 1];
    const squares = current.squares.slice();

    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = xIsNext ? "X" : "O";
    setHistory(tmpHistory.concat([{ squares, squareIndex: i }]));
    setStepNumber(tmpHistory.length);
    setXIsNext(!xIsNext);
    setSelectedStepSquareIndex();
  }

  function jumpTo(step) {
    setStepNumber(step);
    setXIsNext(step % 2 === 0);
    setSelectedStepSquareIndex(history[step]["squareIndex"]);
  }

  function calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (
        squares[a] &&
        squares[a] === squares[b] &&
        squares[a] === squares[c]
      ) {
        return { combination: lines[i], user: squares[a] };
      }
    }
    if (!squares.includes(null)) return 0;
    return null;
  }

  useEffect(() => {
    const tmpHistory = history;

    const moves = tmpHistory.map((step, move) => {
      const line = Math.ceil((step["squareIndex"] + 1) / 3);
      const col = (step["squareIndex"] + 1) % 3 || 3;

      const desc = move
        ? "Перейти к ходу #" + move + " (" + line + ", " + col + ")"
        : "К началу игры";

      return (
        <li key={move}>
          <button
            className={
              step["squareIndex"] === selectedStepSquareIndex ? "selected" : ""
            }
            onClick={() => jumpTo(move)}
          >
            {desc}
          </button>
        </li>
      );
    });

    if (movesOrderDirection === "desc") {
      setOrderedMoves(moves.slice(0, 1).concat(moves.slice(1).reverse()));
    } else {
      setOrderedMoves(moves);
    }
    // eslint-disable-next-line
  }, [history, movesOrderDirection, selectedStepSquareIndex]);

  const tmpHistory = history;
  const current = tmpHistory[stepNumber];
  const winner = calculateWinner(current.squares);

  let status;
  if (winner) {
    status = "Выиграл " + winner.user;
  } else if (winner === 0) {
    status = "У вас ничья!";
  } else {
    status = "Следующий ход: " + (xIsNext ? "X" : "O");
  }

  return (
    <div className="game">
      <div className="game-board">
        <Board
          selectedStepSquareIndex={selectedStepSquareIndex}
          squares={current.squares}
          onClick={(i) => handleClick(i)}
          winnerCombination={winner ? winner.combination : []}
        />
      </div>

      <div className="game-info">
        <div>{status}</div>
        <MyButton
          onClick={() => {
            const direction = movesOrderDirection === "asc" ? "desc" : "asc";
            setMovesOrderDirection(direction);
          }}
        >
          Поменять сортировку ходов
        </MyButton>
        <br />
        <br />
        <ol>{orderedMoves}</ol>
      </div>
    </div>
  );
}

export default Game;
