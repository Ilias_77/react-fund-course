import React, { useState } from "react";
import ColorPalette from "../components/ColorPalette";

import MyButton from "../components/UI/button/MyButton";
import MyInput from "../components/UI/input/MyInput";

const Color = () => {
  // const colors = [
  //   {
  //     borderColor: "red",
  //     textColor: "#7D2223",
  //     name: "Red 10",
  //     value: "#FFECED",
  //   },
  //   {
  //     textColor: "#7D2223",
  //     name: "Red 20",
  //     value: "#FEDADA",
  //   },
  //   {
  //     textColor: "#7D2223",
  //     name: "Red 30",
  //     value: "#FDC7C8",
  //   },
  //   {
  //     textColor: "#7D2223",
  //     name: "Red 40",
  //     value: "#FDA2A3",
  //   },
  //   {
  //     textColor: "#7D2223",
  //     name: "Red 50",
  //     value: "#FB7475",
  //   },
  //   {
  //     name: "Red 60",
  //     value: "#FA4547",
  //   },
  //   {
  //     name: "Red 70",
  //     value: "#E13E40",
  //   },
  // ];
  const [colors, setColors] = useState([
    {
      borderColor: "red",
      textColor: "#7D2223",
      name: "Red 10",
      value: "#FFECED",
    },
    {
      textColor: "#7D2223",
      name: "Red 20",
      value: "#FEDADA",
    },
    {
      textColor: "#7D2223",
      name: "Red 30",
      value: "#FDC7C8",
    },
    {
      textColor: "#7D2223",
      name: "Red 40",
      value: "#FDA2A3",
    },
    {
      textColor: "#7D2223",
      name: "Red 50",
      value: "#FB7475",
    },
    {
      name: "Red 60",
      value: "#FA4547",
    },
    {
      name: "Red 70",
      value: "#E13E40",
    },
  ]);
  const [color, setColor] = useState({
    name: "",
    value: "",
    textColor: "",
    borderColor: "",
  });

  const addNewColor = (e) => {
    e.preventDefault();
    setColors([...colors, color]);
    console.log(color);

    setColor({ name: "", value: "", textColor: "", borderColor: "" });
  };

  return (
    <div>
      <form action="">
        <MyInput
          type="text"
          placeholder="Название цвета"
          value={color.name}
          onChange={(e) => setColor({ ...color, name: e.target.value })}
        />
        <MyInput
          type="text"
          placeholder="Значение цвета"
          value={color.value}
          onChange={(e) => setColor({ ...color, value: e.target.value })}
        />
        <MyInput
          type="text"
          placeholder="Цвет текста"
          value={color.textColor}
          onChange={(e) => setColor({ ...color, textColor: e.target.value })}
        />
        <MyInput
          type="text"
          placeholder="Цвет бородера"
          value={color.borderColor}
          onChange={(e) => setColor({ ...color, borderColor: e.target.value })}
        />
        <MyButton onClick={addNewColor}>Создать пост</MyButton>
      </form>

      <div className="colors">
        <ColorPalette colors={colors} />
      </div>
    </div>
  );
};

export default Color;
