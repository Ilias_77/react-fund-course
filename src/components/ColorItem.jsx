import React from "react";

function ColorItem({ name, value, textColor, borderColor }) {
  textColor = textColor ?? "white";
  borderColor = borderColor ?? value;
  return (
    <div
      className="color__item"
      style={{
        backgroundColor: value,
        color: textColor,
        borderColor: borderColor,
      }}
    >
      <div className="color__name">{name}</div>
      <div>{value}</div>
    </div>
  );
}

export default ColorItem;
