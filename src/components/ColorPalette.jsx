import React from "react";
import ColorItem from "./ColorItem";

const ColorPalette = ({ colors }) => {
  console.log(colors);
  return (
    <div className="color-palitra">
      {colors.map((color) => (
        <ColorItem
          name={color.name}
          value={color.value}
          textColor={color.textColor}
          borderColor={color.borderColor}
          key={color.name}
        />
      ))}
    </div>
  );
};

export default ColorPalette;
