import React from "react";
import classes from "./MyButton.module.sass";

const MyButton = ({ children, ...props }) => {
  return (
    <button {...props} className={`${classes.myBtn} ${classes.active}`}>
      {children}
    </button>
  );
};

export default MyButton;
