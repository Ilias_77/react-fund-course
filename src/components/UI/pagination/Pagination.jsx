import React from "react";
import { getPagesArray } from "../../utils/pages";

const Pagination = ({ page, totalPages, changePage }) => {
  // через UseMemo сделать, чтобы массив не пересчитывался на каждом рендере
  // можно сделать для этого хук usePagination
  let pagesArray = getPagesArray(totalPages); //из числа количества страниц сосдаем массив страниц

  return (
    <div className="page__pagination">
      {pagesArray.map((p) => (
        <span
          key={p}
          onClick={() => {
            changePage(p);
          }}
          className={
            page === p
              ? "page__pagination-item page__pagination-item-current"
              : "page__pagination-item"
          }
        >
          {p}
        </span>
      ))}
    </div>
  );
};

export default Pagination;
