import React from "react";
import Square from "./Square";
import styles from "./Board.module.sass";

function Board(props) {
  function renderSquare(i) {
    return <Square isWinner={props.winnerCombination.includes(i)} isSelected={(props.selectedStepSquareIndex == i)} value={props.squares[i]} onClick={() => props.onClick(i)} />;
  }
  const rowsCount = 3;
  const colsCount = 3;

  return (
    <div>
    {[...Array(rowsCount).keys()].map(row => (
      <div key={row} className={styles.boardRow}>
        {[...Array(colsCount).keys()].map(col => (
          <div key={row * rowsCount + col}>{renderSquare(row * rowsCount + col)}</div>          
        ))}
      </div>
    ))}
    </div>
  );
}

export default Board;
