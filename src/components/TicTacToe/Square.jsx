import React from "react";
import styles from "./Square.module.sass";

function Square({ value, onClick, isSelected, isWinner }) {
  return (
    <button className={styles.square +
      " " +
      (isSelected ? styles.selected : "") +
      " " +
      (isWinner ? styles.winner : "")} onClick={() => onClick()}>
      {value}
    </button>
  );
}

export default Square;
